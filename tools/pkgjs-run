#!/bin/bash

set -e

NOFAILONMISSING=no
if test "$1" = "--if-exist"; then
	shift
	NOFAILONMISSING=yes
fi

TARGET=$1
shift

ARGS=""
FOUND=0
while [ "$1" != "" ]; do
        if [ "$1" == "--" ]; then
                FOUND=1
        elif [ $FOUND != 0 ]; then
                ARGS="$ARGS $1"
        fi
        shift
done

function usage() {
	cat <<EOT
Usage: pkgjs-run <target>

Launch script defined in "package.json -> scripts -> <target>" using sh like
a "npm run".

Features:
 - add node_modules/.bin in PATH
 - ignore git, husky, sudo and su commands
 - replace all "npm run" by pkgjs-run
 - replace all "npm install" by pkgjs-install --ignore --no-download
EOT
}

if test "$TARGET" = ""; then
	usage
	exit 1
fi
if test "$TARGET" = "--help" || test "$TARGET" = "-h"; then
	usage
	exit
fi
if test "$TARGET" = "--version" || test "$TARGET" = "-v"; then
    perl -MDebian::PkgJs::Version -le 'print $VERSION'
    exit
fi

COMMAND=`pkgjs-pjson . scripts $TARGET|perl -pe 's/cross-env//g;s/\bp?npm\b/pkgjs/g;s/\byarn\b/pkgjs/g;s/\blerna\s+run\b/pkgjs-lerna run/g'`
if test "$COMMAND" = ""; then
	echo "Target $TARGET is not defined in package.json" >&2
	if test "$NOFAILONMISSING" = "no"; then
		exit 1
	else
		exit 0
	fi
fi

# Fake git / husky
BADCOMMANDS=${BADCOMMANDS:-"git husky su sudo"}
mkdir -p node_modules/.bin || true
for command in $BADCOMMANDS; do
	rm -f node_modules/.bin/$command
	ln -s /usr/bin/true node_modules/.bin/$command
done

# Aliases
ALIASES=${ALIASES:-"babel,/usr/bin/babeljs yarn,/usr/bin/pkg npm,/urn/bin/pkg pnpm,/usr/bin/pkg"}
for pair in $ALIASES; do
	rm -f node_modules/.bin/${pair%,*}
	ln -s ${pair#*,} node_modules/.bin/${pair%,*}
	BADCOMMANDS="$BADCOMMANDS ${pair%,*}"
done

NAME=`pkgjs-pjson . name`
VERSION=`pkgjs-pjson . version`

if test "$VERSION" != ""; then
	NAME="$NAME@$VERSION"
fi

# Main: launch wanted command
export PATH="node_modules/.bin:$PATH"
CODE=0

# Same output than "npm run"
echo
if test "$NAME" != ""; then
	echo "> $NAME $TARGET"
fi
echo "> $COMMAND $ARGS"
echo
sh -c "KEEP_COMMAND_LINKS=1 $COMMAND $ARGS" || CODE=$?

# Clean our stuff
if test "$KEEP_COMMAND_LINKS" = ""; then
	for command in $BADCOMMANDS; do
		rm -f node_modules/.bin/$command
	done
	rmdir node_modules/.bin 2>/dev/null || true
	rmdir node_modules 2>/dev/null || true
fi
exit $CODE
